## Requirements
- c++ compiler >= 11
- CMake >= 3.11

## Build
```shell
cmake .
cmake --build .
```

## Proof
![Executing the code on Dione cluster](/examples/screen1.jpg)
### Table
I asked only for dione since we did requests earlier for another courses. One execution per measurement.
Measurements are in **milliseconds**.
#### Starting timer before sending message
```cpp
double start, end;
start = MPI_Wtime();
std::cout << "[Main] Sending message to p1 and launch timer" << std::endl;
MPI_Send(buffer, messageSize, MPI_CHAR, 1, 0, MPI_COMM_WORLD);
```


|X|Processors||||
|---|----|--|--|--|
|**Message size**|2|4|8|16
|**100K**|0.37|1|1.19|3.44|
|**10M**|8.38|27.82|48.38|97.42|
|**100M**|75.58|220.57|443.89|860.3|
|**1G**|810.75|2168|4185|8303|

#### Starting timer after sending message
```cpp
std::cout << "[Main] Sending message to p1 and launch timer" << std::endl;
MPI_Send(buffer, messageSize, MPI_CHAR, 1, 0, MPI_COMM_WORLD);
double start, end;
start = MPI_Wtime();
```


|X|Processors||||
|---|----|--|--|--|
|**Message size**|2|4|8|16
|**100K**|0.035|0.45|1.13|1.69|
|**10M**|3.08|15.87|38.76|86.40|
|**100M**|29.8|139.77|347.23|792.78|
|**1G**|296.4|1367|3522|7589|

/**
 * @file main.cpp
 *
 * @author Fabien CAYRE (fabiencayre81@gmail.com)
 *
 * ! IMPORTANT >>>>>>>>>>>>>>>>>>>>>
 * https://gitlab.com/up6tech/S9_Erasmus/Parallel_Programming/ex1_ring_passing
 * @brief
 * @version 0.1
 * @date 2022-09-24
 *
 * @copyright Copyright (c) 2022
 *
 */

#include <iostream>
#include <mpi.h>
#define K 1024       /* One Kilobyte */
#define M K *K       /* One Megabyte */
#define MAXSIZE K *M /* One Gigabyte */

/**
 * MAIN
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[]) {

  MPI_Init(&argc, &argv);

  int process_count, process_id;
  MPI_Comm_size(MPI_COMM_WORLD, &process_count);
  MPI_Comm_rank(MPI_COMM_WORLD, &process_id);

  if (process_count < 2) {
    std::cerr << "Use at least 2 process to execute this programm" << std::endl;
    MPI_Finalize();
    exit(1);
  }

  if (process_id == 0) {
    // MASTER PROCESS
    auto buffer = (char *)malloc(MAXSIZE * sizeof(char));
    if (buffer == NULL) {
      std::cerr << "[Main] Could not allocate " << MAXSIZE << " bytes."
                << std::endl;
      MPI_Finalize();
      exit(0);
    }

    // Init
    for (int i = 0; i < MAXSIZE; i++) {
      buffer[i] = (char)'F';
    }

    while (1) {
      int messageSize = 0;
      std::cout << "[Main] Enter message length (in bytes) or 0 to quit:"
                << std::endl;
      std::cin >> messageSize;
      if (messageSize > MAXSIZE) {
        std::cerr << "[Main] Message too large, maxvalue is " << MAXSIZE
                  << std::endl;
        messageSize = 0;
      }
      // Allocating size on all others cores (not timed)
      for (size_t process_index = 1; process_index < process_count;
           process_index++) {
        MPI_Send(&messageSize, 1, MPI_INT, process_index, 0, MPI_COMM_WORLD);
      }
      if (messageSize <= 0)
        break;
      std::cout << "[Main] Sending message size " << std::endl;
      // Send data to p1
      std::cout << "[Main] Sending message to p1 and launch timer" << std::endl;
      MPI_Send(buffer, messageSize, MPI_CHAR, 1, 0, MPI_COMM_WORLD);
      double start, end;
      start = MPI_Wtime();
      // Waiting message from p(process_count)-1
      MPI_Recv(buffer, messageSize, MPI_CHAR, process_count - 1, 0,
               MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      end = MPI_Wtime();
      std::cout << "[Main] Took " << ((end - start) * 1000)
                << "ms for message to pass over " << (process_count - 1)
                << " processes" << std::endl;
    }

    free(buffer);
    MPI_Finalize();
    exit(0);
  } else {
    // SLAVE PROCESS
    int messageSize = 0;

    while (1) {
      // Get size from p0
      MPI_Recv(&messageSize, 1, MPI_INT, 0, 0, MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);
      if (messageSize == 0) {
        std::cout << "[P" << process_id << "] Terminating ..." << std::endl;
        break;
      }
      std::cout << "[P" << process_id << "] "
                << "Allocating buffer of size " << messageSize
                << "bytes on process " << process_id << std::endl;
      // Allocate a buffer of size size
      auto buffer = (char *)malloc(messageSize * sizeof(char));

      // Wait for message from p-1
      MPI_Recv(buffer, messageSize, MPI_CHAR, process_id - 1, 0, MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);
      std::cout << "[P" << process_id << "] "
                << "Process " << process_id << "/" << process_count
                << " received buffer " << std::endl;

      std::cout << "[P" << process_id << "] "
                << "Sending data to P" << ((process_id + 1) % process_count)
                << std::endl;
      // Send message to n+1
      MPI_Send(buffer, messageSize, MPI_CHAR, (process_id + 1) % process_count,
               0, MPI_COMM_WORLD);
      free(buffer);
    }
  }

  MPI_Finalize();

  return 0;
}
